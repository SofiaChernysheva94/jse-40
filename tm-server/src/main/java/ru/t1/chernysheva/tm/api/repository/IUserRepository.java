package ru.t1.chernysheva.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.model.User;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm_user (id, login, email, " +
            "first_name, last_name, middle_name, role, password_hash)" +
            " VALUES (#{user.id}, #{user.login}, #{user.email},  " +
            "#{user.firstName}, #{user.lastName}, #{user.middleName}, #{user.role}, #{user.passwordHash})")
    void add(@NotNull @Param("user") User user);

    @Delete("DELETE FROM tm_user")
    void clear();

    @Select("SELECT * FROM tm_user")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable List<User> findAll();

    @Select("SELECT * FROM tm_user WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable User findOneById(@NotNull @Param("id") String id);

    @Select("SELECT * FROM tm_user WHERE LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable User findOneByIndex(@NotNull @Param("index") Integer index);

    @Select("SELECT COUNT(*) FROM tm_user")
    int getSize();

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void remove(@NotNull User user);

    @Update("UPDATE tm_user SET login = #{login}, password_hash = #{passwordHash}, email = #{email}, " +
            "locked = #{locked}, first_name = #{firstName}, last_name = #{lastName}, " +
            "middle_name = #{middleName}, role = #{role} WHERE id = #{id}")
    void update(@NotNull User user);

    @Select("SELECT * FROM tm_user WHERE login = #{login} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable User findByLogin(@NotNull @Param("login") String login);

    @Select("SELECT * FROM tm_user WHERE email = #{email} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    @Nullable User findByEmail(@NotNull @Param("email") String email);


}
