package ru.t1.chernysheva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.api.endpoint.IUserEndpoint;
import ru.t1.chernysheva.tm.api.service.IAuthService;
import ru.t1.chernysheva.tm.api.service.IServiceLocator;
import ru.t1.chernysheva.tm.api.service.IUserService;
import ru.t1.chernysheva.tm.dto.request.user.*;
import ru.t1.chernysheva.tm.dto.response.user.*;
import ru.t1.chernysheva.tm.enumerated.Role;
import ru.t1.chernysheva.tm.exception.AbstractException;
import ru.t1.chernysheva.tm.model.Session;
import ru.t1.chernysheva.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.security.NoSuchAlgorithmException;

@WebService(endpointInterface = "ru.t1.chernysheva.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserService getUserService() {
        return this.getServiceLocator().getUserService();
    }

    @NotNull
    @Override
    @WebMethod
    public UserChangePasswordResponse changeUserPassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserChangePasswordRequest request
    ) throws Exception {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String password = request.getPassword();
        try {
            getUserService().setPassword(userId, password);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e);
        }
        return new UserChangePasswordResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserLockResponse lockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLockRequest request
    ) throws Exception {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        try {
            getUserService().lockUserByLogin(login);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e);
        }
        return new UserLockResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserUnlockResponse unlockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUnlockRequest request
    ) throws Exception {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        try {
            getUserService().unlockUserByLogin(login);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e);
        }
        return new UserUnlockResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserRemoveResponse removeUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserRemoveRequest request
    ) throws Exception {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        try {
            getUserService().removeByLogin(login);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e);
        }
        return new UserRemoveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserRegistryResponse registryUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserRegistryRequest request
    ) throws Exception {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        @NotNull final User user = authService.registry(login, password, email);
        return new UserRegistryResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse showUserProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserProfileRequest request
    ) throws Exception {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = getUserService().findOneById(userId);
        return new UserProfileResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserUpdateProfileResponse updateUserProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUpdateProfileRequest request
    ) throws Exception {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();
        try {
            getUserService().updateUser(userId, firstName, lastName, middleName);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e);
        }
        return new UserUpdateProfileResponse();
    }

}
