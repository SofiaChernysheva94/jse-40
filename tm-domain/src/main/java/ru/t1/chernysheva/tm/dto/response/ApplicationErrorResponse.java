package ru.t1.chernysheva.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.exception.system.AbstractSystemException;

@Getter
@Setter
public class ApplicationErrorResponse extends AbstractResultResponse {

    public ApplicationErrorResponse() {
        setSuccess(false);
    }

    public ApplicationErrorResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
