package ru.t1.chernysheva.tm.exception.system;

public class AccessDeniedException extends AbstractSystemException{

    public AccessDeniedException() {
        super("Error! You are not logged in. Please log in and try again...");
    }

}
