package ru.t1.chernysheva.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class ApplicationAboutResponse extends AbstractResponse {

    @Nullable
    private String applicationName;

    @Nullable
    private String authorName;

    @Nullable
    private String authorEmail;

    @Nullable
    private String gitBranch;

    @Nullable
    private String gitCommitId;

    @Nullable
    private String gitCommitterName;

    @Nullable
    private String gitCommitterEmail;

    @Nullable
    private String gitCommitMessage;

    @Nullable
    private String gitCommitTime;

}
