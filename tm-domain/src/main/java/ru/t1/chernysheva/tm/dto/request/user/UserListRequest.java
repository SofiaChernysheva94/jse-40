package ru.t1.chernysheva.tm.dto.request.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.request.AbstractUserRequest;

public final class UserListRequest extends AbstractUserRequest {

    public UserListRequest(@Nullable final String token) {
        super(token);
    }

}
