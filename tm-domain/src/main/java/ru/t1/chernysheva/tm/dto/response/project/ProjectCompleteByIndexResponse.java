package ru.t1.chernysheva.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
public class ProjectCompleteByIndexResponse extends AbstractProjectResponse {

    public ProjectCompleteByIndexResponse(@NotNull final Project project) {
        super(project);
    }

}
