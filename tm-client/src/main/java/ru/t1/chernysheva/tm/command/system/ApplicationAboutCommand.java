package ru.t1.chernysheva.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.dto.request.ApplicationAboutRequest;
import ru.t1.chernysheva.tm.dto.response.ApplicationAboutResponse;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private final String ARGUMENT = "-a";

    @NotNull
    private final String NAME = "about";

    @NotNull
    private final String DESCRIPTION = "Show developer info.";

    @Override
    public void execute() {
        @NotNull final ApplicationAboutRequest request = new ApplicationAboutRequest();
        @NotNull final ApplicationAboutResponse response = getSystemEndpoint().getAbout(request);

        System.out.println("[APPLICATION]");
        System.out.println("NAME: " + response.getApplicationName());
        System.out.println();

        System.out.println("[ABOUT]");
        System.out.println("AUTHOR: " + response.getAuthorName());
        System.out.println("E-MAIL: " + response.getAuthorEmail());
        System.out.println();

        System.out.println("[GIT]");
        System.out.println("BRANCH: " + response.getGitBranch());
        System.out.println("COMMIT ID: " + response.getGitCommitId());
        System.out.println("COMMITTER NAME: " + response.getGitCommitterName());
        System.out.println("COMMITTER EMAIL: " + response.getGitCommitterEmail());
        System.out.println("MESSAGE: " + response.getGitCommitMessage());
        System.out.println("TIME: " + response.getGitCommitTime());
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
